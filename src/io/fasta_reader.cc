/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fasta_reader.hpp"

#include <cctype>


namespace dfp { namespace io {

	void fasta_reader::open( const std::string &filename ) // throws dfp::errors::fasta_reader_exception
	{
		m_filename = filename;
		m_ifs.open(filename);
		
		if(!m_ifs)
		{
			std::stringstream ss;
			ss << "Could not open fasta file: '" << filename << "'";
			throw dfp::errors::fasta_reader_exception(ss.str());
		}
	}


	bool fasta_reader::read_fasta_id( std::string& name ) // throws dfp::errors::fasta_reader_exception
	{
		int c = m_ifs.peek();
		
		while( m_ifs.good() and (c == ' ' or c == '\n' or c == '\r') )
		{
			m_ifs.ignore(1);
			if( m_ifs.good() ) c = m_ifs.peek();
		}
		
		if( m_ifs.good() and c != '>' )
		{
			std::stringstream ss;
			ss << "read_fasta_id: character '" << c << "' found, '>' was expected";
			throw dfp::errors::fasta_reader_exception(ss.str());
		}
		
		// end of file reached
		if( m_ifs.eof() ) 
			return false;
		
		std::getline( m_ifs, name ); // get fasta sequence header
		name = name.substr(1);       // remove '>' character
		
		// discard everything after the first white space
		size_t pos = name.find_first_of(" \t");
		if( pos != std::string::npos ) 
			name = name.substr(0,pos);
		
		// if the identifier is empty, throw an exception
		if( name == "" ) 
			throw dfp::errors::fasta_reader_exception("read_fasta_id: empty identifier");
		
		return true;
	}


	size_t fasta_reader::read_fasta_sequence( std::string& seq ) // throws dfp::errors::fasta_reader_exception
	{
		seq.clear();
		int c = m_ifs.peek();
		
		while( m_ifs.good() and (c == ' ' or c == '\n' or c == '\r') )
		{
			m_ifs.ignore(1);
			if( m_ifs.good() ) c = m_ifs.peek();
		}
		
		size_t seq_size{0};
		while( m_ifs.good() and c != '>' )
		{
			c = m_ifs.get();
			
			if( c != '>' and c != ' ' and c != '\n' and c != '\r' and !m_ifs.eof() )
			{
				if( not isalpha(c) ) 
					throw dfp::errors::fasta_reader_exception("read_fasta_sequence: incorrect FASTA sequence");
				
				seq.push_back(toupper(c));
			}
		}
		
		if( m_ifs.good() ) 
			m_ifs.unget();
		
		if( seq.size() == 0 ) 
			throw dfp::errors::fasta_reader_exception("read_fasta_sequence: found empty sequence");
		
		return seq_size;
	}

} } // namespace dfp::io
