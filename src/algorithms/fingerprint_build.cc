/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fingerprint_build.hpp"

#include <list>
#include <thread>

#include "algorithms/seq_utils.hpp"
#include "data_structures/sequence_kmer.hpp"
#include "utils/blocktimer.hpp"

namespace dfp {

    namespace details {

        /*****************************/
        /* frequencyblock definition */
        /*****************************/

        /*frequencyblock::frequencyblock() 
        { }*/

        frequencyblock::frequencyblock( dfp::kmer kmer, size_t pos, uint64_t freq ) : 
            m_first(kmer),
            m_last(kmer),
            m_start{pos},
            m_end{pos+dfp::kmer::k()},
            m_freq{freq}
        { }

        const dfp::kmer&
        frequencyblock::first() const
        {
            return m_first;
        }

        const dfp::kmer&
        frequencyblock::last() const
        {
            return m_last;
        }

        size_t
        frequencyblock::start() const
        {
            return m_start;
        }

        size_t
        frequencyblock::end() const
        {
            return m_end;
        }

        size_t
        frequencyblock::freq() const
        {
            return m_freq;
        }

        size_t 
        frequencyblock::length() const
        {
            assert( end() >= start() + dfp::kmer::k() );
            return end()-start();
        }

        frequencyblock& 
        frequencyblock::extend( const frequencyblock &rhs )
        {
            assert( rhs.freq() == this->freq() );
            assert( rhs.start() > this->start() and rhs.end() > this->end() );

            m_last = rhs.last();
            m_end  = rhs.end();
            m_data.insert( m_data.end(), rhs.m_data.begin(), rhs.m_data.end() );
            return *this;
        }

        frequencyblock&
        frequencyblock::extend( dfp::kmer kmer, size_t pos )
        {
            assert( pos > this->start() and pos + dfp::kmer::k() > this->end() );
            
            m_last = kmer;
            m_end  = pos + dfp::kmer::k();
            return *this;
        }

        void 
        frequencyblock::add( dfp::kmer kmer, size_t pos )
        {
            m_data.emplace_back(kmer,pos);
        }


        const std::vector<frequencyblock::data_type>& 
        frequencyblock::internal_kmers() const
        {
            return m_data;
        }


        /**********************************/
        /* auxiliary functions definition */
        /**********************************/

        std::vector<size_t> 
        fill_interval( size_t begin, size_t end, uint32_t gap )
        {
            std::vector<size_t> fill_pos;

            if( end <= begin+gap ) // interval satisfies gap constraint
                return fill_pos;

            if( end-begin <= 2*dfp::kmer::k()+1 ) // interval is still too short
                return fill_pos;

            size_t n = end-begin;
            size_t m = begin + (n%2 == 0 ? n>>1 : (n+1)>>1);

            fill_pos = fill_interval( begin, m-dfp::kmer::k(), gap );
            
            fill_pos.push_back( m - dfp::kmer::k() );
            fill_pos.push_back( n%2 == 0 ? m : m+1 );
            
            auto rfill = fill_interval( m+dfp::kmer::k()+1, end, gap );

            fill_pos.insert( fill_pos.end(), rfill.begin(), rfill.end() );

            return fill_pos;
        }


        void 
        fingerprint_build_thr( const dfp::kmerhashtable& kht, dfp::io::wholesequence_parser& parser, dfp::fpvec& fingerprints, const dfp::options& opt )
        {
            const auto ht_ary = kht.ary();
            
            dfp::sequence_kmer       mers;     // note: non-canonical k-mer iterator
            const dfp::sequence_kmer mers_end;
                
            size_t tmp_id;
            dfp::kmerhashtable::array::key_type tmp_key;

            uint64_t freq{0};
            const size_t min_adj_kmers{3};
            std::list< dfp::details::frequencyblock > int_list;
            
            while(true) 
            {
                dfp::io::wholesequence_parser::job seq(parser);
                
                if(seq.is_empty()) 
                    break;

                // init fingerprint
                auto& seqfp = fingerprints.at(seq->gid());
                seqfp.seqlen(seq->size());

                // skip short sequences
                if( seq->size() < std::max(opt.min_block_len,dfp::kmer::k()) ) 
                {
                    if( opt.verbose ) 
                    {
                        std::cerr << "[warning] sequence \"" << seq->name() << "\" (" << seq->pid() << "." << seq->sid() << ") is too short, "
                            << "size=" << seq->size() << " min=" << std::max(opt.min_block_len,dfp::kmer::k()) 
                            << "\n\n";
                    }

                    continue;
                }

                if( opt.verbose ) 
                {
                    std::cerr << "[" << __func__ << "] processing sequence " << seq->gid() << " (" << seq->pid() << "." << seq->sid() << ")"
                              << " name=\"" << seq->name() << "\""
                              << "\n";
                }

                // seek const-frequency intervals

                for( mers = *seq; mers != mers_end; )
                {
                    // retrive frequency from the hash table
                    ht_ary->get_val_for_key( mers->get_canonical(), &freq, tmp_key, &tmp_id );
                    assert( freq > 0 and "k-mer frequency cannot be 0!!!");
                    assert( mers.pos() >= 0 and mers.pos() <= seq->length()-dfp::kmer::k() and "wrong k-mer position!!!" );

                    frequencyblock fblock(*mers, mers.pos(), freq); 
                    
                    // extend the interval with consecutive k-mers as much as possible
                    bool extended;
                    do
                    {
                        extended = false;
                        if( ++mers != mers_end )
                        {
                            ht_ary->get_val_for_key( mers->get_canonical(), &freq, tmp_key, &tmp_id );

                            if( freq == fblock.freq() and mers.pos() == fblock.end()-dfp::kmer::k()+1 )
                            {
                                extended = true;
                                fblock.extend(*mers,mers.pos());
                            }
                        }
                    }
                    while( mers != mers_end and extended );

                    // process the interval only if it is made by min_adj_kmers consecutive k-mers
                    if( fblock.length() + 1 >= dfp::kmer::k() + min_adj_kmers )
                    {
                        if( opt.verbose ) 
                        {
                            std::cerr << "[" << __func__ << "] found interval [" << fblock.start() << "," << fblock.end() << ") of consecutive k-mers "
                                      << "with freq=" << fblock.freq() << " and len=" << fblock.length() << "\n";
                        }

                        // fill interval w.r.t. max-kmer-gap constraint
                        if( opt.max_kmer_gap > 2*dfp::kmer::k() + 1 )
                        {
                            auto fill_pos = fill_interval( fblock.start()+dfp::kmer::k(), fblock.end()-dfp::kmer::k(), opt.max_kmer_gap );
                            for( auto z : fill_pos ) fblock.add( dfp::kmer_at(seq->data(),z), z );
                        }

                        // try to extend a previous interval, while processing those which cannot be extended no more
                        bool merged{false};
                        auto b = int_list.begin();
                        while( b != int_list.end() )
                        {
                            // FIX: consider opt.max_kmer_gap also here!!!
                            if( fblock.start() <= b->end() + opt.max_block_gap ) // interval is still in scope
                            {
                                // TODO: try to extend intervals using a looser condition
                                // TODO: store more than two k-mer in intervals (e.g., when the gap between the first and last one is greater than a threshold)
                                if( fblock.freq() == b->freq() )
                                {
                                    b->extend(fblock);
                                    merged = true;
                                }

                                ++b;
                            }
                            else // interval is out of scope
                            {
                                if( b->length() >= opt.min_block_len )
                                {
                                    // PE kmer
                                    /*const auto& mid = b->internal_kmers();
                                    if( mid.size() > 0 )
                                    {
                                        auto pmer1 = std::make_pair( b->first(), mid.front().first );
                                        auto ppos1 = std::make_pair( b->start(), mid.front().second );
                                        seqfp.add( {pmer1,ppos1} );

                                        for( size_t i{0}; i+1 < mid.size(); ++i )
                                        {
                                            auto pmer = std::make_pair( mid[i].first, mid[i+1].first );
                                            auto ppos = std::make_pair( mid[i].second, mid[i+1].second );
                                            seqfp.add( {pmer,ppos} );
                                        }

                                        auto pmer2 = std::make_pair( mid.back().first, b->last() );
                                        auto ppos2 = std::make_pair( mid.back().second, b->end()-dfp::kmer::k() );
                                        seqfp.add( {pmer2,ppos2} );
                                    }
                                    else
                                    {
                                        auto pmer = std::make_pair( b->first(), b->last() );
                                        auto ppos = std::make_pair( b->start(), b->end()-dfp::kmer::k() );
                                        seqfp.add( {pmer,ppos} );
                                    }

                                    if( opt.verbose )
                                        std::cerr << "[" << __func__ << "] updated fingerprint = " << seqfp << "\n";

                                    //auto pmer = std::make_pair( b->first(), b->last() );
                                    //auto ppos = std::make_pair( b->start(), b->end()-dfp::kmer::k() );
                                    //seqfp.add( {pmer,ppos} );*/

                                    // SE kmers
                                    seqfp.add( {b->first(),b->start()} );
                                    seqfp.add( {b->last(),b->end()-dfp::kmer::k()} );
                                    
                                    for( const auto& p : b->internal_kmers() )
                                        seqfp.add({p.first,p.second});
                                }

                                b = int_list.erase(b);
                            }
                        }

                        if( not merged ) // add a new interval
                            int_list.push_back(fblock);
                    }
                }

                // process remaining intervals
                for( auto b = int_list.begin(); b != int_list.end(); b = int_list.erase(b) )
                {
                    if( b->length() >= opt.min_block_len )
                    {
                        // PE kmer
                        /*const auto& mid = b->internal_kmers();
                        if( mid.size() > 0 )
                        {
                            auto pmer1 = std::make_pair( b->first(), mid.front().first );
                            auto ppos1 = std::make_pair( b->start(), mid.front().second );
                            seqfp.add( {pmer1,ppos1} );

                            for( size_t i{0}; i+1 < mid.size(); ++i )
                            {
                                auto pmer = std::make_pair( mid[i].first, mid[i+1].first );
                                auto ppos = std::make_pair( mid[i].second, mid[i+1].second );
                                seqfp.add( {pmer,ppos} );
                            }

                            auto pmer2 = std::make_pair( mid.back().first, b->last() );
                            auto ppos2 = std::make_pair( mid.back().second, b->end()-dfp::kmer::k() );
                            seqfp.add( {pmer2,ppos2} );
                        }
                        else
                        {
                            auto pmer = std::make_pair( b->first(), b->last() );
                            auto ppos = std::make_pair( b->start(), b->end()-dfp::kmer::k() );
                            seqfp.add( {pmer,ppos} );
                        }

                        if( opt.verbose )
                            std::cerr << "[" << __func__ << "] updated fingerprint = " << seqfp << "\n";
                        
                        //auto pmer = std::make_pair( b->first(), b->last() );
                        //auto ppos = std::make_pair( b->start(), b->end()-dfp::kmer::k() );
                        //seqfp.add( {pmer,ppos} );*/

                        // SE kmers
                        seqfp.add( {b->first(),b->start()} );
                        seqfp.add( {b->last(),b->end()-dfp::kmer::k()} );

                        for( const auto& p : b->internal_kmers() )
                            seqfp.add({p.first,p.second});
                    }
                }

                // sort chosen k-mers by position and add it to the fingerprints collection
                seqfp.sort();

                if( opt.verbose )
                    std::cerr << "[" << __func__ << "] fingerprint computed = " << seqfp << "\n";

            } // end of while
        }

    } // namespace details



    void fingerprint_build( const dfp::kmerhashtable& kht, dfp::sequence_store& seqstore, dfp::fpvec& fingerprints, const dfp::options& opt )
    {
        utils::blocktimer timer(__func__);

        size_t nthreads{ opt.nthreads };
        std::vector<std::thread> threads(nthreads);

        //dfp::io::wholesequence_stream_manager wssm( seqstore.begin(), seqstore.end(), 4 * nthreads );
        dfp::io::wholesequence_parser wsp( seqstore.begin(), seqstore.end(), 4 * nthreads );
        
        for( auto& t : threads ) 
            t = std::thread( details::fingerprint_build_thr, std::cref(kht), std::ref(wsp), std::ref(fingerprints), std::cref(opt) );
        
        for( auto& t : threads )
            t.join();
    }
    

} // namespace detfp
