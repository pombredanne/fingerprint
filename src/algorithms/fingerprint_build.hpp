/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <utility>

#include "common/jf_includes.hpp"
#include "data_structures/fingerprint.hpp"
#include "data_structures/sequence.hpp"
#include "io/wholesequence_stream.hpp"
#include "options/options.hpp"


namespace dfp {
	
	namespace details {

		/*****************************/
		/* auxiliary data structures */
		/*****************************/
		
		class frequencyblock
	    {

	    	using data_type = std::pair<dfp::kmer,size_t>;

	    public:

	        //frequencyblock();
	        frequencyblock( dfp::kmer kmer, size_t pos, uint64_t f );

	        const data_type& front() const;
	        const data_type& back()  const;

	        const dfp::kmer& first() const;
	        const dfp::kmer& last()  const;

	        size_t start()  const;
	        size_t end()    const;
	        size_t freq()   const;
	        size_t length() const;

	        // extend block
	        frequencyblock& extend( dfp::kmer kmer, size_t pos );
	        frequencyblock& extend( const frequencyblock &block );

	        void add( dfp::kmer kmer, size_t pos );
	        const std::vector<data_type>&  internal_kmers() const;

	        void sort();

	    private:

	    	dfp::kmer m_first;
	    	dfp::kmer m_last;
	        std::vector<data_type> m_data;
	        
	        size_t m_start{0};
	        size_t m_end{0};
	        size_t m_freq{0};
	    };

	    /***********************/
	    /* auxiliary functions */
	    /***********************/

	    std::vector<size_t> fill( size_t begin, size_t end, uint32_t gap );

	    void fingerprint_build_thr( const dfp::kmerhashtable& kht, dfp::io::wholesequence_parser& parser, dfp::fpvec& fingerprints, const dfp::options& opt );

	} // namespace details

	

	void fingerprint_build( const dfp::kmerhashtable& kht, dfp::sequence_store& seqstore, dfp::fpvec& fingerprints, const dfp::options& opt );

} // namespace detfp
