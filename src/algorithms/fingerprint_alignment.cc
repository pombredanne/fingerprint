/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "fingerprint_alignment.hpp"

#include <thread>
#include <limits>

#include "third_party/bwa/ksw.h"
#include "utils/blocktimer.hpp"
#include "utils/functional.hpp"

namespace dfp {
	
	namespace details {

        /**********************/
        /* kmerhit definition */
        /**********************/

        kmerhit::kmerhit( size_t qry_gid, size_t trg_gid, size_t qry_idx, size_t trg_idx ) :
        	m_qry_gid{qry_gid},
        	m_qry_idx{qry_idx},
        	m_trg_gid{trg_gid},
        	m_trg_idx{trg_idx}
        { }

        size_t 
        kmerhit::qry_gid() const { return m_qry_gid; }

        size_t 
        kmerhit::qry_idx() const { return m_qry_idx; }
        
        size_t 
        kmerhit::trg_gid() const { return m_trg_gid; }
        
        size_t 
        kmerhit::trg_idx() const { return m_trg_idx; }

        bool 
        kmerhit::operator<( const kmerhit& rhs ) const
        {
        	return (this->qry_gid() < rhs.qry_gid()) 
        	    or (this->qry_gid() == rhs.qry_gid() and this->trg_gid() < rhs.trg_gid())
                or (this->qry_gid() == rhs.qry_gid() and this->trg_gid() == rhs.trg_gid() and this->qry_idx() < rhs.qry_idx())
                or (this->qry_gid() == rhs.qry_gid() and this->trg_gid() == rhs.trg_gid() and this->qry_idx() == rhs.qry_idx() and trg_idx() < rhs.trg_idx());
        }


        /************************/
        /* kmerchain definition */
        /************************/

        kmerchain::kmerchain( size_t qry_gid, size_t trg_gid, size_t hits, bool rev ) :
        	m_qry_gid{qry_gid},
        	m_trg_gid{trg_gid},
        	m_hits{hits},
        	m_rev{rev}
        { }

        kmerchain::kmerchain( size_t qry_gid, size_t trg_gid, size_t hits, bool rev, size_t qry_idx, size_t trg_idx ) :
        	m_qry_gid{qry_gid},
        	m_trg_gid{trg_gid},
        	m_hits{hits},
        	m_rev{rev}
        { 
        	this->add(qry_idx,trg_idx);
        }

        size_t 
        kmerchain::qry_gid() const { return m_qry_gid; }

        size_t 
        kmerchain::trg_gid() const { return m_trg_gid; }

        size_t 
        kmerchain::hits() const { return m_hits; }

        bool
        kmerchain::rev() const { return m_rev; }

        const std::vector<size_t>& 
        kmerchain::qry_chain() const { return m_qry_chain; }
        
        const std::vector<size_t>& 
        kmerchain::trg_chain() const { return m_trg_chain; }

        size_t 
        kmerchain::qry_first() const { return m_qry_chain.front(); }
        
        size_t 
        kmerchain::trg_first() const { return m_trg_chain.front(); }

        size_t 
        kmerchain::qry_last() const { return m_qry_chain.back(); }
        
        size_t 
        kmerchain::trg_last() const { return m_trg_chain.back(); }

        size_t 
        kmerchain::size() const { return m_qry_chain.size(); }

        void 
        kmerchain::add( size_t qry_idx, size_t trg_idx )
        {
        	m_qry_chain.push_back(qry_idx);
        	m_trg_chain.push_back(trg_idx);
        }
        

	    /***********************/
	    /* auxiliary functions */
	    /***********************/

	    // build fingerprint kmer table
		kmervec
		kmer_table_build( const fpvec& fingerprints, bool filter_dup )
		{
			utils::blocktimer timer(__func__);
			kmervec kmertable;

			// fill vector with fingerprint k-mers
			for( size_t gid{0}; gid < fingerprints.size(); ++gid )
			{
				const auto& fp = fingerprints[gid];

				for( size_t idx{0}; idx < fp.size(); ++idx )
			        kmertable.emplace_back( fp.kmer(idx).get_canonical(), gid, idx );
			}

			std::cerr << "[" << __func__ << "] kmer table size = " << kmertable.size() << std::endl;
			
			// sort k-mer table
			std::sort( kmertable.begin(), kmertable.end() );
				
			// possibly remove k-mers which are not unique within a fingerprint
			if( filter_dup )
			{
				auto eq_pred = []( const kmerrec &a, const kmerrec& b ){ return (a.kmer() == b.kmer() and a.gid() == b.gid()); };
				auto first_dup = utils::strict_unique( kmertable.begin(), kmertable.end(), eq_pred );

				// actually remove k-mers
				kmertable.erase( first_dup, kmertable.end() );
				std::cerr << "[" << __func__ << "] table size after duplicate removal = " << kmertable.size() << std::endl;
			}

			return kmertable;
		}


		hitvec
		hit_table_build( const sequence_store& seqstore, const kmervec& kmertable, bool self_align )
		{
			utils::blocktimer timer(__func__);
			
			hitvec hit_table;
			
			for( size_t i{0}; i < kmertable.size(); ++i )
			{
				// skip hits between a sequence and itself
				size_t j{i}; do { ++j; } while( j < kmertable.size() and kmertable[i].gid() == kmertable[j].gid() );

				while( j < kmertable.size() and kmertable[i].kmer() == kmertable[j].kmer() )
				{
					if( self_align or seqstore.at(kmertable[i].gid()).pid() < seqstore.at(kmertable[j].gid()).pid() )
						hit_table.emplace_back( kmertable[i].gid(), kmertable[j].gid(), kmertable[i].idx(), kmertable[j].idx() );

					++j;
				}
			}

			std::cerr << "[" << __func__ << "] hit table size = " << hit_table.size() << " (no-self=" << (!self_align ? "true" : "false") << ")" << std::endl;

			// sort hit table
			std::sort( hit_table.begin(), hit_table.end() );

			return hit_table;
		}


		// TODO: controllare casi particolari (e.g., ci sono problemi se ht_size < num_threads)
	    chainvec
	    cluster_hits( const fpvec& fingerprints, const hitvec& hit_table, const dfp::options& opt )
	    {
	    	utils::blocktimer timer(__func__);

	    	size_t ht_size    = hit_table.size();
	        size_t block_size = ht_size/opt.nthreads;

	        if( block_size == 0 ) // this should not happen
	        {
	            std::cerr << "[" << __func__ << "] error: block_size = 0 (this should not happen!)" <<  std::endl;
	            return chainvec();
	        }

	        std::vector<chainvec>    thr_chains(opt.nthreads);
	        std::vector<std::thread> threads(opt.nthreads);

	        for( size_t start{0}, tid{0}; start < ht_size; )
	        {
	            // set end of the interval for thread tid (last thread handles last records)
	            size_t end = tid+1 == opt.nthreads ? ht_size : std::min(ht_size,start+block_size);

	            // increase end such that the following hit refers to a pair of sequences
	            size_t qry_gid = hit_table[end-1].qry_gid();
	            size_t trg_gid = hit_table[end-1].trg_gid();
	            while( end < ht_size and qry_gid == hit_table[end].trg_gid() and trg_gid == hit_table[end].trg_gid() ) end++;

	            // start thread
	            threads[tid] = std::thread( cluster_hits_thr, std::cref(fingerprints), std::cref(hit_table), start, end, std::ref(thr_chains[tid]), std::cref(opt) );

	            tid++;
	            start = end;
	        }

	        // wait for threads
	        for( auto& t : threads ) 
	            t.join();

	        // compute number of chains
	        size_t num_chains{0}; 
	        std::for_each( thr_chains.begin(), thr_chains.end(), [&](const chainvec& tc){ num_chains += tc.size();} );
	        std::cerr << "[" << __func__ << "] found " << num_chains << " putative overlaps." << std::endl;

	        // merge thread chains in a single one
	        chainvec chains; chains.reserve(num_chains);
	        std::for_each( thr_chains.begin(), thr_chains.end(), [&](chainvec& tc){ chains.insert( chains.end(), tc.begin(), tc.end() ); } );

	        return chains;
	    }


	    void 
	    cluster_hits_thr( const fpvec& fingerprints, const hitvec& hit_table, size_t start, size_t end, chainvec& chains, const dfp::options& opt )
	    {
	        chains.reserve( (end-start)/4 );

	        size_t i, j;

	        std::vector<size_t> qry_fwd;
	        std::vector<size_t> qry_rev;
	        
	        std::vector<size_t> trg_fwd;
	        std::vector<size_t> trg_rev;

	        for( size_t i=start; i < end; i=j )
	        {
	            size_t qry_gid = hit_table[i].qry_gid();
	            size_t trg_gid = hit_table[i].trg_gid();

	            const auto& qry_fp = fingerprints.at(qry_gid);
	            const auto& trg_fp = fingerprints.at(trg_gid);

	            for( j=i; j < end and qry_gid == hit_table[j].qry_gid() and trg_gid == hit_table[j].trg_gid(); ++j )
	            {
	                if( qry_fp.kmer(hit_table[j].qry_idx()) == trg_fp.kmer(hit_table[j].trg_idx()) ) // forward hit
	                {
	                    qry_fwd.emplace_back( hit_table[j].qry_idx() );
	                    trg_fwd.emplace_back( hit_table[j].trg_idx() );
	                }
	                else // reverse-complement hit
	                {
	                    qry_rev.emplace_back( hit_table[j].qry_idx() );
	                    trg_rev.emplace_back( hit_table[j].trg_idx() );
	                }
	            }

	            if( opt.use_lss )
	            {
	            	if( qry_fwd.size() >= opt.min_shared_hits )
		            {
		            	kmerchain chn( qry_gid, trg_gid, qry_fwd.size(), false );
			            
			            auto lss = find_lss<true>(trg_fwd);
			            for( size_t z : lss ) chn.add( qry_fwd[z], trg_fwd[z] );

			            chains.push_back(chn);
		            }

		            if( qry_rev.size() >= opt.min_shared_hits )
		            {
		            	kmerchain chn( qry_gid, trg_gid, qry_rev.size(), true );
			            
			            auto lss = find_lss<false>(trg_rev);
			            for( size_t z : lss ) chn.add( qry_rev[z], trg_rev[z] );

			            chains.push_back(chn);
		            }
	            }
	            else
	            {
		            if( qry_fwd.size() >= opt.min_shared_hits )
		                seek_chains_2<true>( fingerprints, qry_gid, qry_fwd, trg_gid, trg_fwd, chains, opt );   

		            if( qry_rev.size() >= opt.min_shared_hits )
		                seek_chains_2<false>( fingerprints, qry_gid, qry_rev, trg_gid, trg_rev, chains, opt );
	        	}
	            

	            qry_fwd.clear();
	            qry_rev.clear();
	            trg_fwd.clear();
	            trg_rev.clear();
	        }
	    }


	    // TODO: sarebbe meglio allocare una volta sola il reverse della query (invece che ogni volta la sequenza reference)
	    alignment align_chain( const kmerchain& chn, const sequence_store& seqstore, const fpvec& fingerprints, const options& opt )
	    {
	        using namespace dfp::constants;

	        const auto& qry_seq = seqstore.at(chn.qry_gid());
	        const auto& trg_seq = seqstore.at(chn.trg_gid());

	        const auto& qry_fp = fingerprints.at(chn.qry_gid());
	        const auto& trg_fp = fingerprints.at(chn.trg_gid());

	        size_t qry_len = qry_seq.length();
	        size_t trg_len = trg_seq.length();

	        size_t q1 = chn.qry_first();
	        size_t q2 = chn.qry_last();
	        size_t t1 = chn.trg_first();
	        size_t t2 = chn.trg_last();

	        size_t qry_beg = qry_fp.pos(q1);
	        size_t qry_end = qry_fp.pos(q2) + qry_fp.at(q2).ins_size();

	        size_t trg_beg = chn.rev() ? trg_fp.pos(t2) : trg_fp.pos(t1);
	        size_t trg_end = chn.rev() ? 
	                         trg_fp.pos(t1) + trg_fp.at(t1).ins_size():
	                         trg_fp.pos(t2) + trg_fp.at(t2).ins_size();

	        ssize_t gap_qry = qry_end - qry_beg;
	        ssize_t gap_trg = trg_end - trg_beg;
	        ssize_t gap_diff = gap_qry > gap_trg ? gap_qry-gap_trg : gap_trg-gap_qry;

	        if( not opt.use_sw )
	        {
	        	double score = std::min(gap_qry,gap_trg) / static_cast<double>(std::max(gap_qry,gap_trg));

	        	return dfp::alignment( chn.qry_gid(), qry_beg, qry_end, qry_len, 
	        				chn.trg_gid(), trg_beg, trg_end, trg_len,
	        				chn.size(), score, chn.rev() );
	        }

	        base_t *qp = const_cast<base_t*>(qry_seq.data());
	        base_t *tp = const_cast<base_t*>(trg_seq.data());

	        if( chn.rev() )
	        {
	        	tp = new base_t[trg_len];
	            std::memcpy( tp, trg_seq.data(), trg_len );
	            reverse_complement(tp,trg_len);
	            reverse_coordinates( trg_beg, trg_end, trg_len );
	        }

	        // best achievable score in the identified overlap region
	        int w = 0;
	        int score = std::min(gap_qry,gap_trg)*MATCH - (gap_diff > 0 ? GAP_OPEN : 0) - gap_diff*GAP_EXT;

	        /*if( score <= 0 ) 
	        {
	        	if( chn.rev() ) delete[] tp;
	        	return dfp::alignment(); 
	        }*/

	        if( qry_beg > 0 and trg_beg > 0 ) // left extension
	        {
	            base_t *as, *bs;
	            int ale, ble, gble, gscore, max_off, new_score;

	            bool swapped = qry_beg > trg_beg;
	            
	            as = new base_t[qry_beg];
	            for( int i = 0; i < qry_beg; ++i ) as[i] = qp[qry_beg-1-i];

	            bs = new base_t[trg_beg];
	            for( int i = 0; i < trg_beg; ++i ) bs[i] = tp[trg_beg-1-i];

	            if( swapped )
	            {
	                std::swap(as,bs);
	                std::swap(qry_beg,trg_beg);
	            }

	            int32_t oh = std::min(qry_beg,trg_beg) * (1.0 - opt.min_identity);
	            int prev_score{ std::numeric_limits<int>::min() };

	            // ATTENZIONE! il valore iniziale di lbw deve essere <= opt.sw_bw
	            for( int32_t lbw = std::min({oh,50,opt.sw_bw}); lbw <= opt.sw_bw; lbw +=50 )
	            {
	                new_score = ksw_extend( qry_beg, as, trg_beg, bs, 5, SW_MAT, GAP_OPEN, GAP_EXT, lbw, 5, 100, score, &ale, &ble, &gble, &gscore, &max_off );
	                if( prev_score == new_score || max_off < (lbw >> 1) + (lbw >> 2) ) break; // max_off < 3/4 * band-width
	                prev_score = new_score;
	            }
	            
	            w = std::max(max_off,w);
	            
	            // choose best alignment
	            //if( gscore <= 0 ) //|| gscore <= new_score-5 ) // local extension
	            //{
	            //    qry_beg -= ale;
	            //    trg_beg -= ble;
	            //    score = new_score;
	            //} 
	            //else // to-end alignment
	            //{ 
	                qry_beg = 0;
	                trg_beg -= gble;
	                score = gscore;
	            //}

	            if( swapped )
	            {
	                std::swap(as,bs);
	                std::swap(qry_beg,trg_beg);
	            }

	            delete[] as;
	            delete[] bs;
	        }


	        /*if( score <= 0 )
	        {
	        	if( chn.rev() ) delete[] tp;
	        	return dfp::alignment(); 
	        }*/


	        if( qry_end != qry_len and trg_end != trg_len ) // right extension
	        {
	            int ale, ble, gble, gscore, max_off, new_score;

	            bool swapped = qry_len-qry_end > trg_len-trg_end;
	            int32_t oh = std::min(qry_len-qry_end,trg_len-trg_end) * (1.0 - opt.min_identity);
	            int prev_score{ std::numeric_limits<int>::min() };

	            if( swapped )
	            {
	                std::swap(qp,tp);
	                std::swap(qry_end,trg_end);
	                std::swap(qry_len,trg_len);
	            }

	            // ATTENZIONE! il valore iniziale di lbw deve essere <= opt.sw_bw 
	            for( int32_t lbw = std::min({oh,50,opt.sw_bw}); lbw <= opt.sw_bw; lbw +=50 )
	            {
	                new_score = ksw_extend( qry_len-qry_end, qp+qry_end, trg_len-trg_end, tp+trg_end, 5, SW_MAT, GAP_OPEN, GAP_EXT, lbw, 5, 100, score, &ale, &ble, &gble, &gscore, &max_off );
	                if( prev_score == new_score || max_off < (lbw >> 1) + (lbw >> 2) ) break; // max_off < 3/4 * band-width
	                prev_score = new_score;
	            }

	            w = std::max(max_off,w);
	            
	            // choose best alignment
	            //if( gscore <= 0 ) //|| gscore <= new_score-5 ) // local extension
	            //{
	            //    qry_end += ale;
	            //    trg_end += ble;
	            //    score = new_score;
	            //} 
	            //else // to-end alignment
	            //{ 
	                qry_end = qry_len;
	                trg_end += gble;
	                score = gscore;
	            //}

	            if( swapped )
	            {
	                std::swap(qp,tp);
	                std::swap(qry_end,trg_end);
	                std::swap(qry_len,trg_len);
	            }
	        }

	        // TODO: try to comment/uncomment the following line which discards the alignment if overhangs are longer than 100 tp
	        //if( std::min(qry_beg,trg_beg) > 100 || std::min(qry_len-qry_end,trg_len-trg_end) > 100 ) return Alignment();
	        if( std::min(qry_beg,trg_beg) > opt.max_overhang or std::min(qry_len-qry_end,trg_len-trg_end) > opt.max_overhang ) 
	        {
	        	if( chn.rev() ) delete[] tp;
	        	return dfp::alignment();
	        }

	        // update region boundaries

	        gap_qry = qry_end - qry_beg;
	        gap_trg = trg_end - trg_beg;
	        gap_diff = gap_qry > gap_trg ? gap_qry-gap_trg : gap_trg-gap_qry;

	        if( opt.fast_mode )
	        {
	        	if( chn.rev() )
		        {
		            reverse_coordinates( trg_beg, trg_end, trg_len );
		            delete[] tp;
		        }
	        	
	        	double score = std::min(gap_qry,gap_trg) / static_cast<double>(std::max(gap_qry,gap_trg));

	        	return dfp::alignment( chn.qry_gid(), qry_beg, qry_end, qry_len, 
	        				chn.trg_gid(), trg_beg, trg_end, trg_len,
	        				chn.size(), score, chn.rev() );
	        }

	        // perform banded-global alignment on the main overlap region

	        //int w = static_cast<int>(std::max(gap_qry,gap_trg) * (1.0-opt.min_identity)); // + 1;
	        //int w = static_cast<int>(gap_diff); //std::min( static_cast<int32_t>(gap_diff), opt.sw_bw << 2 );
	        w = std::max(w,static_cast<int>(gap_diff<<2));

	        int n_cigar{0};
	        uint32_t *cigar{nullptr};

	        int prev_score{ std::numeric_limits<int>::min() };
	        double identity{0.0};

	        // TODO: provare a considerare i match con N nel calcolo dell'identità
	        for( int32_t lbw = w; lbw <= std::max(opt.sw_bw,w); lbw += 50 )
	        {
	            int new_score = ksw_global( qry_end-qry_beg, qp+qry_beg, trg_end-trg_beg, tp+trg_beg, 5, SW_MAT, GAP_OPEN, GAP_EXT, lbw, &n_cigar, &cigar );

	            size_t es_len{0}, es_matches{0}, op_len{0};
	            for( size_t k{0}, i{qry_beg}, j{trg_beg}; k < n_cigar; ++k ) // print CIGAR
	            {
	                //if( opt.verbose ) std::cerr << (cigar[k]>>4) << "MIDSH"[cigar[k]&0xf];
	                op_len = cigar[k]>>4;

	                switch("MIDSH"[cigar[k]&0xf])
	                {
	                    case 'M':
	                        for( int z{0}; z < cigar[k]>>4; ++z, ++i, ++j )
	                            if( qp[i] == tp[j] ) // proper match
	                                ++es_matches;
	                        es_len += op_len;
	                        break;
	                    case 'I':
	                        i += op_len;
	                        es_len += op_len;
	                        break;
	                    case 'D':
	                        j += op_len;
	                        es_len += op_len;
	                        break;
	                    default:
	                        break;
	                }
	            }

	            // free cigar
	            free(cigar);
	            identity = es_len > 0 ? static_cast<double>(es_matches)/es_len : 0.0;

	            if( identity >= opt.min_identity or prev_score >= new_score ) break;
	            prev_score = new_score;
	        }

	        if( chn.rev() )
	        {
	            reverse_coordinates( trg_beg, trg_end, trg_len );
	            delete[] tp;
	        }

	        return dfp::alignment( chn.qry_gid(), qry_beg, qry_end, qry_len, 
	        	                   chn.trg_gid(), trg_beg, trg_end, trg_len,
	        	                   chn.size(), identity, chn.rev() );
	    }


	} // namespace details

	
	alignvec 
	fingerprint_alignment( const sequence_store& seqstore, const fpvec& fingerprints, const options& opt )
	{
		utils::blocktimer timer(__func__);

		auto kmertable = details::kmer_table_build( fingerprints, opt.filter_dup );
		auto hit_table = details::hit_table_build( seqstore, kmertable, opt.self_align );
		auto chains    = details::cluster_hits( fingerprints, hit_table, opt );

		alignvec alignments(chains.size());
		auto aln_fun = [&]( const details::kmerchain& chn ) { return details::align_chain( chn, seqstore, fingerprints, opt ); };
		utils::pmap( chains.begin(), chains.end(), alignments.begin(), aln_fun, opt.nthreads );

		return alignments;
	}
	

} // namespace detfp
