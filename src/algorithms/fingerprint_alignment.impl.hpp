/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <list>


#include "algorithms/seq_utils.hpp"

namespace dfp {
	
	namespace details {

		/**********************/
        /* kmerrec_t definition */
        /**********************/
	    
	    template< typename KT >
	    kmerrec_t<KT>::kmerrec_t( const kmer_type& k, size_t gid, size_t idx ) : 
	    	m_kmer(k),
	    	m_gid(gid),
	    	m_idx(idx) 
	    { }

	    template< typename KT >
	    const typename kmerrec_t<KT>::kmer_type& 
	    kmerrec_t<KT>::kmer() const { return m_kmer; }

	    template< typename KT >
	    size_t 
	    kmerrec_t<KT>::gid() const { return m_gid; }

	    template< typename KT >
	    size_t 
	    kmerrec_t<KT>::idx() const { return m_idx; }

	    template< typename KT >
	    bool 
	    kmerrec_t<KT>::operator<( const kmerrec_t<KT>& rhs ) const 
        { 
            return (m_kmer < rhs.m_kmer)
                or (m_kmer == rhs.m_kmer and m_gid < rhs.m_gid)
                or (m_kmer == rhs.m_kmer and m_gid == rhs.m_gid and m_idx < rhs.m_idx);
        }


		/***********************/
		/* auxiliary functions */
		/***********************/


        template< bool SEEK_FWD_OLP >
	    void 
	    seek_chains( const fpvec& fingerprints, 
	    	         size_t qry_id, std::vector<size_t>& qry_hits,
	    	         size_t trg_id, std::vector<size_t>& trg_hits,
	    	         chainvec& chains, const dfp::options& opt )
	    {
	        std::vector<kmerchain> fnd_chains;

	        const auto& qry_fp = fingerprints.at(qry_id);
	        const auto& trg_fp = fingerprints.at(trg_id);

	        for( size_t i=0; i < qry_hits.size(); ++i ) // for each fwd hit
	        {
	            bool extended{false};

	            for( auto c = fnd_chains.rbegin(); c != fnd_chains.rend(); ++c )
	                extended = test_and_extend<SEEK_FWD_OLP>( *c, qry_fp, trg_fp, qry_hits[i], trg_hits[i] ) or extended;

	            if( not extended )
	            {
	                fnd_chains.emplace_back( qry_id, trg_id, qry_hits.size(), not SEEK_FWD_OLP, qry_hits[i], trg_hits[i] );

	                //std::cerr << "[seek_chain] built chain " << fnd_chains.size() << " <" << qry_id << "," << trg_id << "," << (SEEK_FWD_OLP ? "+> " : "-> ")
	                //          << " on intervals [" << qry_fp.pos(qry_hits[i]) << "," << qry_fp.pos(qry_hits[i])+qry_fp.at(qry_hits[i]).ins_size() << ")"
	                //          << " and [" << trg_fp.pos(trg_hits[i]) << "," << trg_fp.pos(trg_hits[i])+trg_fp.at(trg_hits[i]).ins_size() << ")\n";
	            }
	        }

	        for( size_t z{0}; z < fnd_chains.size(); ++z ) //for( const auto& chn : fnd_chains )
	        {
	        	const auto& chn = fnd_chains[z];

	        	// compute end points of the current chain

	            size_t qry_beg = qry_fp.pos( chn.qry_first() ); 
	            size_t qry_end = qry_fp.pos( chn.qry_last() ) + qry_fp.at(chn.qry_last()).ins_size(); // dfp::kmer::k();
	            
	            size_t trg_beg = trg_fp.pos( SEEK_FWD_OLP ? chn.trg_first() : chn.trg_last()  ); 
	            size_t trg_end = trg_fp.pos( SEEK_FWD_OLP ? chn.trg_last()  : chn.trg_first() ) + trg_fp.at( SEEK_FWD_OLP ? chn.trg_last()  : chn.trg_first() ).ins_size(); //dfp::kmer::k();
	            if( not SEEK_FWD_OLP ) reverse_coordinates( trg_beg, trg_end, trg_fp.seqlen() );

	            size_t qry_right_len = qry_fp.seqlen() - qry_end;
	            size_t trg_right_len = trg_fp.seqlen() - trg_end;

	            size_t qry_miss_left{0}, qry_miss_right{0}, trg_miss_left{0}, trg_miss_right{0};
	            size_t qry_tmp, b_tmp, b_tmp_e, b_tmp_s, pos;

	            // compute not found hits in left region of query

	            qry_tmp = qry_beg <= trg_beg ? 0 : (qry_beg - trg_beg);
	            for( size_t j=0; j < chn.qry_first(); ++j )
	            {
	            	pos = qry_fp.pos(j);
	                if( pos >= qry_tmp and pos < qry_beg )
	                    ++qry_miss_left;
	            }

	            // compute not found hits in left region of target

	            b_tmp = trg_beg <= qry_beg ? 0 : (trg_beg - qry_beg);
	            b_tmp_s = SEEK_FWD_OLP ? 0 : (chn.trg_first() + 1);
	            b_tmp_e = SEEK_FWD_OLP ? chn.trg_first() : trg_fp.size();
	            for( size_t j{b_tmp_s}; j< b_tmp_e; ++j )
	            {
	            	pos = SEEK_FWD_OLP ?
	            	      trg_fp.pos(j) :
	            	      trg_fp.seqlen()-trg_fp.pos(b_tmp_e-j-1)-trg_fp.at(b_tmp_e-j-1).ins_size(); // dfp::kmer::k();

	                if( pos >= b_tmp and pos < trg_beg ) 
	                    ++trg_miss_left;
	            }

	            // compute not found hits in right region of query

	            qry_tmp = qry_right_len <= trg_right_len ? qry_fp.seqlen() : (qry_end + trg_right_len);
	            for( size_t j=chn.qry_last()+1; j < qry_fp.size(); ++j )
	            {
	            	pos = qry_fp.pos(j) + qry_fp.at(j).ins_size(); // dfp::kmer::k();
	                if( pos > qry_end and pos <= qry_tmp ) 
	                    ++qry_miss_right;
	            }

	            // compute not found hits in right region of target

	            b_tmp = trg_right_len <= qry_right_len ? trg_fp.seqlen() : (trg_end + qry_right_len);
	            b_tmp_s = SEEK_FWD_OLP ? chn.trg_last()+1 : 0;
	            b_tmp_e = SEEK_FWD_OLP ? trg_fp.size() : chn.trg_last();
	            for( size_t j{b_tmp_s}; j < b_tmp_e; ++j )
	            {
	                size_t pos = SEEK_FWD_OLP ?
	                             trg_fp.pos(j) + trg_fp.at(j).ins_size() : // dfp::kmer::k()
	                             trg_fp.seqlen()-trg_fp.pos(b_tmp_e-j-1);
	                
	                if( pos > trg_end and pos <= b_tmp ) 
	                    ++trg_miss_right;
	            }

	            // check whether the chain is good (i.e., might refer to a putative overlap)

	            size_t qry_olp_len = qry_end - qry_beg;
	            size_t trg_olp_len = trg_end - trg_beg;

	            size_t left_len  = std::min(qry_beg,trg_beg);
	            size_t reg_len   = std::max(qry_olp_len,trg_olp_len);
	            size_t right_len = std::min(qry_right_len,trg_right_len);

	            size_t max_matches = left_len + std::min(qry_olp_len,trg_olp_len) + right_len;
	            size_t max_es_len  = left_len + reg_len + right_len;

	            size_t hits_num       = chn.size();
	            size_t miss_num_left  = std::min(qry_miss_left,trg_miss_left);
	            size_t miss_num_right = std::min(qry_miss_right,trg_miss_right);
	            size_t miss_num       = miss_num_left + miss_num_right;

	            double hit_ratio       = static_cast<double>(hits_num)/static_cast<double>(hits_num+miss_num);
	            double hit_ratio_left  = static_cast<double>(hits_num*reg_len)/static_cast<double>(hits_num*reg_len + miss_num_left*left_len);
	            double hit_ratio_right = static_cast<double>(hits_num*reg_len)/static_cast<double>(hits_num*reg_len + miss_num_right*right_len);
	            double oh_ratio        = std::max( static_cast<double>(left_len)/reg_len, static_cast<double>(right_len)/reg_len );
	            
	            bool is_good = left_len + (qry_olp_len+trg_olp_len)/2 + right_len >= 2*opt.min_overlap/3
	                and (static_cast<double>(max_matches)/max_es_len >= opt.min_identity)
	                and (miss_num_left <= opt.max_hit_miss and miss_num_right <= opt.max_hit_miss)
	                and (hit_ratio >= 0.7f or (hit_ratio_left >= 0.7f and hit_ratio_right >= 0.7f) or oh_ratio <= 0.3f);

	            //std::cerr << "[seek_chain] " << (is_good ? "good" : "bad") << " chain " << z+1 << " = [" << left_len << ",(" << qry_olp_len << ";" << trg_olp_len << ")," << right_len << "]"
	            //		  << " ovl=" << (left_len + (qry_olp_len+trg_olp_len)/2 + right_len)
	            //	      << " ident=" << (static_cast<double>(max_matches)/max_es_len)
	            //	      << " miss=" << miss_num_left << ":" << miss_num_right
	            //	      << " hit-ratio=" << hit_ratio << " hit-ratio-left=" << hit_ratio_left << " hit-ratio-right=" << hit_ratio_right 
	            //	      << "\n";

	            if( is_good ) 
	            	chains.push_back(chn);
	    	}
	    }

	    
	    template< bool SEEK_FWD_OLP, typename FP >
	    bool
	    test_and_extend( kmerchain &chn, const FP& qry_fp, const FP& trg_fp, size_t qry_idx, size_t trg_idx )
	    {
	    	size_t qry_first = chn.qry_first();
	        size_t trg_first = chn.trg_first();
	        size_t qry_last  = chn.qry_last();
	        size_t trg_last  = chn.trg_last();

	        if( qry_last >= qry_idx or (SEEK_FWD_OLP ? trg_last >= trg_idx : trg_last <= trg_idx) )
	        	return false;

	        ssize_t qry_beg = qry_fp.pos(qry_idx); ssize_t qry_end = qry_beg + qry_fp.at(qry_idx).ins_size(); // dfp::kmer::k();
	        ssize_t trg_beg = trg_fp.pos(trg_idx); ssize_t trg_end = trg_beg + trg_fp.at(trg_idx).ins_size(); // dfp::kmer::k();

	        ssize_t chn_a_beg = qry_fp.pos(qry_first);
	        ssize_t chn_a_end = qry_fp.pos(qry_last) + qry_fp.at(qry_last).ins_size(); // dfp::kmer::k();
	        
	        ssize_t chn_b_beg = SEEK_FWD_OLP ? trg_fp.pos(trg_first) : trg_fp.pos(trg_last);
	        ssize_t chn_b_end = SEEK_FWD_OLP ? 
	                            trg_fp.pos(trg_last) + trg_fp.at(trg_last).ins_size(): // dfp::kmer::k();
	                            trg_fp.pos(trg_first) + trg_fp.at(trg_first).ins_size(); // dfp::kmer::k();

	        ssize_t gap_a = qry_end - chn_a_beg;
	        ssize_t gap_b = SEEK_FWD_OLP ? (trg_end - chn_b_beg) : (chn_b_end - trg_beg);
	        ssize_t gap_diff = (gap_a > gap_b) ? (gap_a - gap_b) : (gap_b - gap_a);
	        double gap_diff_perc = static_cast<double>(gap_diff)/std::max(gap_a,gap_b);

	        //std::cerr << "[test_and_extend] chain " << (SEEK_FWD_OLP ? "fwd " : "rev ")
	        //          << " on intervals [" << chn_a_beg << "," << chn_a_end << ") and [" << chn_b_beg << "," << chn_b_end << ")"
	        //          << " with paired-kmer on [" << qry_beg << "," << qry_end << ") and [" << trg_beg << "," << trg_end << ")"
	        //          << "\n";

	        double qry_diff = static_cast<double>(chn_a_end - qry_beg) / (qry_end - chn_a_beg);
	        double trg_diff = SEEK_FWD_OLP ?
	                          static_cast<double>(chn_b_end - trg_beg) / (trg_end - chn_b_beg):
	                          static_cast<double>(trg_end - chn_b_beg) / (chn_b_end - trg_beg);

	        double rel_diff = (qry_diff >= trg_diff) ? (qry_diff - trg_diff) : (trg_diff - qry_diff);

	        bool included = qry_end <= chn_a_end and (SEEK_FWD_OLP ? trg_end <= chn_b_end : chn_b_beg <= trg_beg);
	        bool overlapping = qry_beg < chn_a_end and (SEEK_FWD_OLP ? trg_beg < chn_b_end : chn_b_beg < trg_end);

	        //std::cerr << "                  qry-diff=" << qry_diff << " trg-diff=" << trg_diff << " rel-diff=" << rel_diff
	        //		  << "                  included=" << (included ? "1" : "0") << " overlapping=" << (overlapping ? "1" : "0")
	        //		  << "\n";

	        bool good_extension = !included and (overlapping or rel_diff <= 0.05f or gap_diff_perc <= 0.05f);

	        if(good_extension)
	        {
	        	chn.add(qry_idx,trg_idx);
	            return true;
	        }

	        return false;
	    }


	    template< bool SEEK_FWD_OLP >
		void 
		seek_chains_2 (
			const fpvec& fingerprints, 
	    	size_t qry_id, std::vector<size_t>& qry_hits,
			size_t trg_id, std::vector<size_t>& trg_hits,
			chainvec& chains, const dfp::options& opt )
		{
			std::vector<kmerchain> fnd_chains;

	        const auto& qry_fp = fingerprints.at(qry_id);
	        const auto& trg_fp = fingerprints.at(trg_id);

	        for( size_t i=0; i < qry_hits.size(); ++i ) // for each fwd hit
	        {
	            bool   found{false};

	            size_t best_c{fnd_chains.size()};
	            double best_score{1.0f}; // a good score is assumed to be always < 1.0
	            size_t best_qry_idx{qry_hits.size()};
	            size_t best_trg_idx{trg_hits.size()};

	            for( size_t c{0}; c < fnd_chains.size(); ++c )
	            {
	            	double score;

	            	if( try_extension<SEEK_FWD_OLP>( fnd_chains[c], qry_fp, trg_fp, qry_hits[i], trg_hits[i], score ) and score < best_score )
	                {
	                	found        = true;
	                	best_c       = c;
	                	best_score   = score;
	                	best_qry_idx = qry_hits[i];
	                	best_trg_idx = trg_hits[i];
	                }
	            }

	            if(found)
	            {
	            	fnd_chains[best_c].add( best_qry_idx, best_trg_idx );
	            	continue;
	            }

	            fnd_chains.emplace_back( qry_id, trg_id, qry_hits.size(), not SEEK_FWD_OLP, qry_hits[i], trg_hits[i] );
	        }


	        for( const auto& chn : fnd_chains )
	        {
	        	// compute end points of the current chain

	            size_t qry_beg = qry_fp.pos( chn.qry_first() ); 
	            size_t qry_end = qry_fp.pos( chn.qry_last() ) + qry_fp.at(chn.qry_last()).ins_size();
	            
	            size_t trg_beg = trg_fp.pos( SEEK_FWD_OLP ? chn.trg_first() : chn.trg_last()  ); 
	            size_t trg_end = trg_fp.pos( SEEK_FWD_OLP ? chn.trg_last()  : chn.trg_first() ) + trg_fp.at( SEEK_FWD_OLP ? chn.trg_last()  : chn.trg_first() ).ins_size();
	            if( not SEEK_FWD_OLP ) reverse_coordinates( trg_beg, trg_end, trg_fp.seqlen() );

	            size_t qry_olp_len = qry_end - qry_beg;
	            size_t trg_olp_len = trg_end - trg_beg;

	            size_t qry_right_len = qry_fp.seqlen() - qry_end;
	            size_t trg_right_len = trg_fp.seqlen() - trg_end;

	            size_t qry_miss_left{0}, qry_miss_right{0}, trg_miss_left{0}, trg_miss_right{0};
	            size_t qry_tmp, b_tmp, b_tmp_e, b_tmp_s, pos;

	            
	            size_t qry_beg_2 = qry_beg >= 0.3f * qry_olp_len ? qry_beg + 0.3f * qry_olp_len : 0;
	            size_t trg_beg_2 = trg_beg >= 0.3f * trg_olp_len ? trg_beg + 0.3f * trg_olp_len : 0;
	            size_t qry_end_2 = qry_right_len >= 0.3f * qry_olp_len ? qry_end + 0.3f * qry_olp_len : qry_fp.seqlen();
	            size_t trg_end_2 = trg_right_len >= 0.3f * trg_olp_len ? trg_end + 0.3f * trg_olp_len : qry_fp.seqlen();


	            // compute not found hits in left region of query

	            qry_tmp = qry_beg_2 <= trg_beg_2 ? 0 : (qry_beg_2 - trg_beg_2);
	            for( size_t j=0; j < chn.qry_first(); ++j )
	            {
	            	pos = qry_fp.pos(j);
	                if( pos >= qry_tmp and pos < qry_beg_2 )
	                    ++qry_miss_left;
	            }

	            // compute not found hits in left region of target

	            b_tmp = trg_beg_2 <= qry_beg_2 ? 0 : (trg_beg_2 - qry_beg_2);
	            b_tmp_s = SEEK_FWD_OLP ? 0 : (chn.trg_first() + 1);
	            b_tmp_e = SEEK_FWD_OLP ? chn.trg_first() : trg_fp.size();
	            for( size_t j{b_tmp_s}; j< b_tmp_e; ++j )
	            {
	            	pos = SEEK_FWD_OLP ?
	            	      trg_fp.pos(j) :
	            	      trg_fp.seqlen()-trg_fp.pos(b_tmp_e-j-1)-trg_fp.at(b_tmp_e-j-1).ins_size(); // dfp::kmer::k();

	                if( pos >= b_tmp and pos < trg_beg_2 ) 
	                    ++trg_miss_left;
	            }

	            // compute not found hits in right region of query

	            qry_tmp = qry_fp.seqlen()-qry_end_2 <= trg_fp.seqlen()-trg_end_2 ? qry_fp.seqlen() : (qry_end_2 + trg_fp.seqlen() - trg_end_2);
	            for( size_t j=chn.qry_last()+1; j < qry_fp.size(); ++j )
	            {
	            	pos = qry_fp.pos(j) + qry_fp.at(j).ins_size(); // dfp::kmer::k();
	                if( pos > qry_end and pos <= qry_tmp ) 
	                    ++qry_miss_right;
	            }

	            // compute not found hits in right region of target

	            b_tmp = trg_fp.seqlen()-trg_end_2 <= qry_fp.seqlen()-qry_end_2 ? trg_fp.seqlen() : (trg_end_2 + qry_fp.seqlen()-qry_end_2);
	            b_tmp_s = SEEK_FWD_OLP ? chn.trg_last()+1 : 0;
	            b_tmp_e = SEEK_FWD_OLP ? trg_fp.size() : chn.trg_last();
	            for( size_t j{b_tmp_s}; j < b_tmp_e; ++j )
	            {
	                size_t pos = SEEK_FWD_OLP ?
	                             trg_fp.pos(j) + trg_fp.at(j).ins_size() : // dfp::kmer::k()
	                             trg_fp.seqlen()-trg_fp.pos(b_tmp_e-j-1);
	                
	                if( pos > trg_end_2 and pos <= b_tmp ) 
	                    ++trg_miss_right;
	            }

	            // check whether the chain is good (i.e., might refer to a putative overlap)
	            size_t left_len  = std::min(qry_beg,trg_beg);
	            size_t reg_len   = std::max(qry_olp_len,trg_olp_len);
	            size_t right_len = std::min(qry_right_len,trg_right_len);

	            size_t max_matches = left_len + std::min(qry_olp_len,trg_olp_len) + right_len;
	            size_t max_es_len  = left_len + reg_len + right_len;

	            size_t hits_num       = chn.size();
	            size_t miss_num_left  = std::min(qry_miss_left,trg_miss_left);
	            size_t miss_num_right = std::min(qry_miss_right,trg_miss_right);
	            size_t miss_num       = miss_num_left + miss_num_right;

	            double hit_ratio       = static_cast<double>(hits_num)/static_cast<double>(hits_num+miss_num);
	            double hit_ratio_left  = static_cast<double>(hits_num*reg_len)/static_cast<double>(hits_num*reg_len + miss_num_left*left_len);
	            double hit_ratio_right = static_cast<double>(hits_num*reg_len)/static_cast<double>(hits_num*reg_len + miss_num_right*right_len);
	            double oh_ratio        = std::max( static_cast<double>(left_len)/reg_len, static_cast<double>(right_len)/reg_len );
	            
	            bool is_good = left_len+reg_len+right_len >= 2*opt.min_overlap/3                                               // putative overlap >= 2/3 min-overlap threshold
	                and (static_cast<double>(max_matches)/max_es_len >= opt.min_identity)                                      // best identity we can get >= min-identity threshold
	                and (miss_num_left <= opt.max_hit_miss and miss_num_right <= opt.max_hit_miss)                             // not too much hits not found outside the overlapping region
	                and (static_cast<double>(std::min(qry_olp_len,trg_olp_len))/std::max(qry_olp_len,trg_olp_len) >= 0.7f)     // max relative difference between overlaps < 30%
	                and (oh_ratio <= 0.2f or hit_ratio >= 0.7f or (hit_ratio_left >= 0.7f and hit_ratio_right >= 0.7f));

	            if( is_good ) 
	            	chains.push_back(chn);
	    	}
		}


		template< bool SEEK_FWD_OLP, typename FP >
	    bool
	    try_extension( const kmerchain &chn, const FP& qry_fp, const FP& trg_fp, size_t qry_idx, size_t trg_idx, double& rel_diff )
	    {
	    	size_t qry_first = chn.qry_first();
	        size_t trg_first = chn.trg_first();
	        size_t qry_last  = chn.qry_last();
	        size_t trg_last  = chn.trg_last();

	        if( qry_last >= qry_idx or (SEEK_FWD_OLP ? trg_last >= trg_idx : trg_last <= trg_idx) )
	        	return false;

	        ssize_t qry_beg = qry_fp.pos(qry_idx); ssize_t qry_end = qry_beg + qry_fp.at(qry_idx).ins_size();
	        ssize_t trg_beg = trg_fp.pos(trg_idx); ssize_t trg_end = trg_beg + trg_fp.at(trg_idx).ins_size();

	        ssize_t chn_a_beg = qry_fp.pos(qry_first);
	        ssize_t chn_a_end = qry_fp.pos(qry_last) + qry_fp.at(qry_last).ins_size();
	        
	        ssize_t chn_b_beg = SEEK_FWD_OLP ? trg_fp.pos(trg_first) : trg_fp.pos(trg_last);
	        ssize_t chn_b_end = SEEK_FWD_OLP ? 
	                            trg_fp.pos(trg_last) + trg_fp.at(trg_last).ins_size():
	                            trg_fp.pos(trg_first) + trg_fp.at(trg_first).ins_size();

	        double qry_diff = static_cast<double>(chn_a_end - qry_beg) / (qry_end - chn_a_beg);
	        double trg_diff = SEEK_FWD_OLP ?
	                          static_cast<double>(chn_b_end - trg_beg) / (trg_end - chn_b_beg):
	                          static_cast<double>(trg_end - chn_b_beg) / (chn_b_end - trg_beg);
	        
	        rel_diff = qry_diff >= trg_diff ? qry_diff-trg_diff : trg_diff-qry_diff;

	        bool included = qry_end <= chn_a_end and (SEEK_FWD_OLP ? trg_end <= chn_b_end : chn_b_beg <= trg_beg);
	        bool overlapping = qry_beg < chn_a_end and (SEEK_FWD_OLP ? trg_beg < chn_b_end : chn_b_beg < trg_end);

	        return !included and rel_diff <= 0.05f; //(overlapping or rel_diff <= 0.05f or gap_diff_perc <= 0.05f);
	    }


	} // namespace details

	

	

} // namespace detfp
