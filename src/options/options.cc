/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "options.hpp"

#include <iostream>
#include <chrono>
using std::chrono::system_clock;

#include <boost/program_options.hpp>
namespace boost_po = boost::program_options;
#include <boost/filesystem.hpp>
namespace boost_fs = boost::filesystem;

#include "common/constants.hpp"
#include "errors/exit_exception.hpp"


const char* DFP_OVERLAP_VERSION = "0.3.3";


namespace dfp 
{

    void options::process( int argc, char** argv )
    {
        std::string command_basename = boost_fs::basename(argv[0]);

        std::stringstream desc_ss;
        desc_ss << "\nProgram: " << command_basename 
                << "\nVersion: " << DFP_OVERLAP_VERSION
                << "\n\nUsage";

        // VISIBLE OPTIONS
        boost_po::options_description user_opts(desc_ss.str().c_str());
        user_opts.add_options()
            ("help,h",                                                                 "produce help message")
            ("version,v",                                                              "print version of the tool")
            ("verbose",                                                                "verbose output")
        // INPUT PARAMETERS
            ("input,i", boost_po::value<std::string>(&this->input_fasta)->required(),  "file in which each line is a path to a fasta file")
            ("k,k", boost_po::value<uint32_t>(&this->k)->required(),                   "size of k-mers (it must be an integer in [5,31]; default:24)")
            ("genome-size,s", boost_po::value<size_t>(&this->genome_size)->required(), "approximate genome size" )
            ("fast",                                                                   "do not compute precisely ES and identity")
            ("lss",                                                                    "use longest strictly increasing/decreasing algorithm to find overlaps" )
            ("min-hits,c", boost_po::value<size_t>(&this->min_shared_hits),            "minimum number of k-mers shared by fingerprints to check the overlap (default:1)" )
            // ("max-freq,f", boost_po::value<int32_t>(&this->max_freq),                  "max k-mer frequency (default: 20)" )
            ("min-identity", boost_po::value<double>(&this->min_identity),             "minimum overlap identity (it must be a value in [0,1]; default: 0.95)" )
            ("min-overlap,l", boost_po::value<int32_t>(&this->min_overlap),            "minimum overlap length (default: 1000)" )
            ("max-overhang", boost_po::value<size_t>(&this->max_overhang),             "max overlap's overhang (it must be a non-negative integer; default: 0)" )
            ("threads,t", boost_po::value<size_t>(&this->nthreads),                    "number of threads (default: 1)" )
        // OUTPUT PARAMETERS
            ("prefix,p", boost_po::value<std::string>(&this->out_prefix),              "output prefix. (default: \"out\")")
        ;

        // HIDDEN OPTIONS
        boost_po::options_description hidden_opts("Hidden options");
        hidden_opts.add_options()
            ("min-block-len", boost_po::value<uint32_t>(&this->min_block_len),  "minimum length of putative overlapping region are retained (default:2k+1)")
            ("max-block-gap", boost_po::value<int32_t>(&this->max_block_gap),   "max gap (bp) for extending \"constant frequency\" regions (default:1)")
            ("max-kmer-gap", boost_po::value<uint32_t>(&this->max_kmer_gap),    "max gap (bp) between fingerprint k-mers in a \"constant frequency\" region (it must be a positive integer; default: 50)")
            ("no-self",                                                         "do not check for overlaps within the same fasta file")
            ("no-sw",                                                           "do not align with Smith-Waterman")
            ("filter-dup",                                                      "discard k-mers which are not locally-unique in a fingerprint")
            ("max-hit-miss", boost_po::value<size_t>(&this->max_hit_miss),      "maximum number of fingerprint k-mers before/after the first/last shared hit (default:5)")
        ;

        boost_po::options_description all_opts("Valid options");
        all_opts.add(user_opts).add(hidden_opts);

        boost_po::variables_map vm;
        try
        {
            boost_po::store(boost_po::parse_command_line(argc, argv, all_opts), vm);

            if( vm.count("help") )
            {
                std::cout << user_opts << std::endl;
                throw dfp::errors::exit_exception(EXIT_SUCCESS);
            }

            if( vm.count("version") )
            {
                std::cout << command_basename << " v" << DFP_OVERLAP_VERSION << std::endl;
                throw dfp::errors::exit_exception(EXIT_SUCCESS);
            }

            boost_po::notify(vm);
        }
        catch (boost_po::error& error)
        {
            std::cerr << "[error] " << error.what() << ", try -h for help." << std::endl;
            throw dfp::errors::exit_exception(EXIT_FAILURE);
        }

        // validate k parameter
        if( this->k < dfp::constants::MIN_KMER || this->k > dfp::constants::MAX_KMER )
        {
            std::cerr << "[error] -k parameter must be an integer between " << dfp::constants::MIN_KMER << " and " << dfp::constants::MAX_KMER << std::endl;
            throw dfp::errors::exit_exception(EXIT_FAILURE);
        }

        // validate min-identity
        if( this->min_identity < 0.0 or this->min_identity > 1.0 )
        {
            this->min_identity = std::max(this->min_identity,0.0);
            this->min_identity = std::min(this->min_identity,1.0);
            std::cerr << "[warning] --min-identity is not in [0,1], setting it to " << this->min_identity << std::endl;
        }

        // validate threads
        if( this->nthreads == 0 )
            this->nthreads = 1;

        // validate min-block-len
        if( vm.count("min-block-len") )
        {
            // validate min-block-len
            if( this->min_block_len <= this->k )
            {
                std::cerr << "[error] --min-block-len parameter must be greater than k" << std::endl;
                throw dfp::errors::exit_exception(EXIT_FAILURE);
            }
        }
        else // set default value if min-block-len is not provided
        {
            this->min_block_len = 1 + 2 * this->k;
        }        

        // validate max-kmer-gap
        if( vm.count("max-kmer-gap") and this->max_kmer_gap <= 2*this->k+1 )
        {
            std::cerr << "[error] --max-kmer-gap parameter must be greater than 2*k+1" << std::endl;
            throw dfp::errors::exit_exception(EXIT_FAILURE);
        }

        // validate min-hits
        if( vm.count("min-hits") and this->min_shared_hits < 1 )
        {
            std::cerr << "[warning] -c (--min-hits) parameter must be positive, setting it to 1" << std::endl;
            this->min_shared_hits = 1;
        }

        if( vm.count("lss") ) 
            this->use_lss = true;

        if( vm.count("no-self") ) 
            this->self_align = false;

        if( vm.count("no-sw") ) 
            this->use_sw = false;

        if( vm.count("fast") ) 
            this->fast_mode = true;

        if( vm.count("filter-dup") )
            this->filter_dup = true;

        if (vm.count("verbose")) 
             this->verbose++;
    }

} // namespace dfp
