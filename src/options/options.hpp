/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdint.h>
#include <string>
#include <vector>


namespace dfp
{

    struct options
    {
        std::string input_fasta = "";
        std::string out_prefix = "out";

        // visible options
        uint32_t k{24};
        int32_t  max_freq{100};
        size_t   genome_size{0};
        size_t   nthreads{1};
        double   min_identity{0.90};
        size_t   min_shared_hits{1};
        size_t   max_overhang{0};
        uint32_t verbose{0};

        // hidden options
        int32_t  sw_bw{200};
        int32_t  min_overlap{1000};
        uint32_t min_block_len{100};
        int32_t  max_block_gap{-5};
        uint32_t max_kmer_gap{0};     // must be >= 2*dfp::kmer::k()+1
        bool     self_align{true};
        bool     filter_dup{false};
        size_t   max_hit_miss{5};
        bool     use_lss{false};
        bool     use_sw{true};
        bool     fast_mode{false};

        options() = default;
        options( const options& ) = default;
        options( int argc, char** argv ) { this->process(argc,argv); }

    private:

        void process( int argc, char** argv );

    };

} // namespace dfp
