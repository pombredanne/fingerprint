/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "algorithms.hpp"

#include <vector>
#include <cassert>

#include "algorithms/seq_utils.hpp"
#include "third_party/bwa/ksw.h"


namespace dfp 
{

    double seed_sw( const dfp::sequence& query, int qb, int qe, const dfp::sequence& target, int tb, int te, bool rev )
    {
        using namespace dfp::constants;
        const int MAX_EXT = 50;

        size_t qlen = query.length();
        size_t tlen = target.length();

        uint8_t *qry = const_cast<uint8_t*>(query.data());
        uint8_t *trg = const_cast<uint8_t*>(target.data());

        qb -= MAX_EXT; qb = qb > 0 ? qb : 0;
        qe += MAX_EXT; qe = qe < qlen ? qe : qlen;
        tb -= MAX_EXT; tb = tb > 0 ? tb : 0;
        te += MAX_EXT; te = te < tlen ? te : tlen;

        if( rev ) 
        {
            trg = new uint8_t[te-tb];
            std::memcpy( trg, target.data()+tb, te-tb );
            dfp::reverse_complement(trg,te-tb);
        }

        kswr_t x = ksw_align( qe-qb, qry+qb, te-tb, rev ? trg : trg+tb, 5, SW_MAT, GAP_OPEN, GAP_EXT, KSW_XSTART, 0);
        
        if( rev ) delete[] trg;

        int maxlen = std::max(qe-qb,te-tb);
        double identity = 1.0 - static_cast<double>(maxlen*MATCH-x.score) / (MATCH + MISMATCH) / maxlen;

        return identity;
    }



} // namespace dfp
