/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <chrono>
#include <string>
#include <sstream>
#include <iostream>

#include "utils/misc.hpp"

namespace utils
{
	
	class blocktimer
	{
		using hrc = std::chrono::high_resolution_clock;
		using timepoint_t = std::chrono::time_point<hrc>;


	public:

		blocktimer( const std::string& name = "" ) : 
			m_name(name), 
			m_start(hrc::now())
		{ }

		~blocktimer()
		{
			auto interval = hrc::now() - m_start;
			auto seconds = std::chrono::duration_cast<std::chrono::seconds>(interval).count();

			std::stringstream ss;
			ss << "[" << m_name << "] execution time = "
				<< utils::human_time(seconds)
				<< std::endl;

			std::cerr << ss.str();
		}


	private:

		std::string m_name;
		timepoint_t m_start;

	};

} // namespace utils

