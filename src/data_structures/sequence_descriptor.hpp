/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <cassert>
#include <cstdlib>
#include <cstdint>

#include <iostream>
#include <vector>
#include <string>

#include "common/constants.hpp"
#include "common/types.hpp"


namespace dfp {

	template< typename store_type >
	class sequence_descriptor
	{
		
	public:
		
		// using StorageType = typename Storage::StorageType;
		// using iterator = typename StorageType::iterator;
		// using const_iterator = typename StorageType::const_iterator;

		
	public:
		
		sequence_descriptor() { };
		
		sequence_descriptor( int32_t pid, int32_t sid, const std::string& name, store_type* storage_ptr, size_t offset, size_t len ) :
			m_pid(pid), 
			m_sid(sid), 
			m_name(name), 
			m_storage_ptr(storage_ptr), 
			m_offset(offset), 
			m_len(len)
		{ }

		sequence_descriptor( const sequence_descriptor& rhs ) = default;
		sequence_descriptor( sequence_descriptor&& )          = delete;   // no need of move-assignment at the moment
		
		sequence_descriptor& operator=( const sequence_descriptor& rhs ) = default;
		sequence_descriptor& operator=( sequence_descriptor&& )          = delete;   // no need of move-assignment at the moment

		// get pool identifier
		inline int32_t pid() const { return m_pid; }

		// get sequence identifier
		inline int32_t sid() const { return m_sid; }
		
		// get sequence (former) name
		inline const std::string& name() const { return m_name; }
		inline const std::string& str_id() const { return name(); }

		// get length of sequence
		inline size_t length() const { return m_len; }
		inline size_t size() const { return length(); }
		
		// get raw pointers to the first base of the sequence
		inline base_t*       data() { return m_storage_ptr->data() + m_offset; }
		inline const base_t* data() const { return m_storage_ptr->data() + m_offset; }
		
		// iterators on sequence data
		// iterator       begin() { return m_storage_ptr->data_begin() + m_offset; }
		// const_iterator begin() const { return m_storage_ptr->data_begin() + m_offset; }
		// iterator       end() { return begin() + m_len; }
		// const_iterator end() const { return begin() + m_len; }

		// get (const) reference to the i-th base of the sequence
		inline base_t&        at( const size_t& i ) { return m_storage_ptr->data_at(m_offset+i); }
		inline const base_t&  at( const size_t& i ) const { return m_storage_ptr->data_at(m_offset+i); }
		inline base_t&        operator[]( const size_t& i ) { return m_storage_ptr->data()[m_offset+i]; }
		inline const base_t&  operator[]( const size_t& i ) const { return m_storage_ptr->data()[m_offset+i]; }

		// return global identifier.
		inline size_t gid() const { return m_storage_ptr->gid(m_pid,m_sid); }

		// get a string representation of the whole sequence
		inline std::string string() const { return this->subseq(0,this->length()); }
		
		// get string representation of the subsequence in interval [start,end)
		std::string subseq( size_t start, size_t end ) const
		{
			assert( start <= end and end <= m_len );
			using namespace dfp::constants;

			std::string out;
			out.reserve(end-start);

			size_t i = start;

			if( start <= end ) 
			{
				for( size_t i=start; i < end; ++i )
					out.push_back( base2char[this->at(i)] );
			}
			
			return out;
		}
		
		// write sequence to an output stream
		friend std::ostream& operator<<( std::ostream& os, const sequence_descriptor<store_type>& sd )
		{
			os << '>' << sd.pid() << "." << sd.sid() // identifier as <pool-id>.<seq-id>
			   << "\tname=" << sd.name()              // report name of the sequence
			   << sd.string();                        // actual sequence
			
			return os;
		}
		
		
	private:
		
		int32_t      m_pid{-1};   // pool id
		int32_t      m_sid{-1};   // sequence id
		std::string  m_name{};    // sequence name
		
		store_type*  m_storage_ptr{nullptr};
		size_t       m_offset{0};            // absolute offset in storage data
		size_t       m_len{0};               // length of sequence
		
	}; // end class Sequence


} // namespace dfp
